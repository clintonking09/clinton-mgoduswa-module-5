import 'package:flutter/material.dart';

void main() => runApp(const SnackBarDemo());

class Design extends StatelessWidget {
  const Design({super.key});

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Welcome to MTN',//change
      home: Scaffold(
        appBar: AppBar(
          title: const Text('Welcome to MTN'),//change
        ),
        body: const DesignPage(),
      ),
    );
  }
}

class DesignPage extends StatelessWidget {
  const DesignPage({super.key});

  @override
  Widget build(BuildContext context) {
    return Center(
      child: ElevatedButton(
        onPressed: () {
          final snackBar = SnackBar(
            content: const Text('Welcome!'),//change
            action: SnackBarAction(
              label: 'Undo',
              onPressed: () {
                // Some code to undo the change.
              },
            ),
          );

       
          ScaffoldMessenger.of(context).showSnackBar(snackBar);
        },
        child: const Text('Login'),
      ),
    );
  }
}
