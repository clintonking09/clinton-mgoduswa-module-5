import 'package:flutter/material.dart';
import 'package:animated_splash/animated_splash.dart';

void main() {
Function duringSplash = () {
	print('Something background process');
	int a = 123 + 23;
	print(a);

	if (a > 100)
	return 1;
	else
	return 2;
};

Map<int, Widget> op = {1: Home(), 2: HomeSt()};

runApp(MaterialApp(
	home: AnimatedSplash(
	imagePath: 'https://play.google.com/store/apps/details?id=com.mtnsar3&hl=en_US&gl=US',//change
	home: Home(),
	customFunction: duringSplash,
	duration: 3000,
	type: AnimatedSplashType.BackgroundProcess,
	outputAndHome: op,
	),
));
}

class Home extends StatefulWidget {
@override
_HomeState createState() => _HomeState();
}

class _HomeState extends State<Home> {
@override
Widget build(BuildContext context) {
	return Scaffold(
		appBar: AppBar(
		title: Text('Welcome to MTN'),//change
		backgroundColor: Colors.black,//change
		),
		body: Center(
			child: Text('My Home Page',
				style: TextStyle(color: Colors.black,//change
					fontSize: 15.0))));//change
}
}
